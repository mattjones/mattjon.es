+++
title = "Notes 11.7.2024"
author = ["Matt Jones"]
date = 2024-07-11
lastmod = 2024-08-11T23:16:02+01:00
tags = ["Film", "Music"]
categories = ["linknotes"]
draft = false
weight = 2002
[[resources]]
  src = "46645-1532336916.jpg"
  title = "Still from the film The Shining. Wendy, played by Shelley Duvall, sits at the table with Danny"
  name = "theshining"
+++

{{< imgproc name="theshining"
    command="Resize"
    options="1200x webp"
    imageCaption="Still from the film The Shining. Wendy, played by Shelley Duvall, sits at the table with Danny"
    imageURL=""
    imageAttrib=""
    imageLicense="Fair Use"
    licenseURL="https://en.wikipedia.org/wiki/Fair_use">}}


## Today's notes {#today-s-notes}

-   [Shelley Duvall dies on her birthday aged 75](/blog/links/2024/07/11#shelley-duval-dies-on-her-birthday-aged-75): Thoughts on Duvall, Kubrick and sexism in the film industry.
-   [Eurorack Pitch Quantizer Module Comparison](/blog/links/2024/07/11#eurorack-pitch-quantizer-module-comparison): Accessible, niche web information for the win.
-   [Musical connection of the day](/blog/links/2024/07/11#musical-connection-of-the-day): Le bassiste sur la chanson thème Last of the Summer Wine.

<!--more-->

---


### Shelley Duval dies on her birthday aged 75 {#shelley-duval-dies-on-her-birthday-aged-75}

<https://www.theguardian.com/film/article/2024/jul/11/shelley-duvall-star-of-the-shining-and-annie-hall-dies-aged-75>

I'm reassessing the way I feel about Kubrick's films, especially The Shining, which starred Shelley Duval as Wendy.

In Vivian Kubrick's 'Making of...' film we see Duvall suffering from exhaustion during the punishing filming schedule and generally being derided and put down by Kubrick, almost as a parallel to what happens in the film.

It's the sexism that we know is engrained in every industry, including the film industry.

---


### Eurorack Pitch Quantizer Module Comparison {#eurorack-pitch-quantizer-module-comparison}

<https://doudoroff.com/quantizers/>

I'm currently planning my first Eurorack modular synth  and one module I need is a pitch quantizer.

A pitch quantizer translates incoming voltages to pitches in a musical scale.

This is useful if you have something generating random voltages and you want the sound this outputs to be tuneful.

THis site lists the Eurorack pitch quantizers available alongside their technical features. It's an example of a site that presents a set of very niche and detailed information in an accessible way, as opposed to the common approach these days which is to put it all into a Youtube video.  This sort of thing is the best of the Web.

---


### Musical connection of the day {#musical-connection-of-the-day}

The bass player on the recording of 'Histoire de Melody Nelson' by Serge Gainsbourg also played bass on the theme tune of the 1980s BBC sitcom Last of the Summer Wine.


[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"