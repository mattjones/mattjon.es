+++
title = "Notes 8.7.2024"
author = ["Matt Jones"]
date = 2024-07-08
lastmod = 2024-08-11T23:16:02+01:00
tags = ["Environment", "Politics"]
categories = ["linknotes"]
draft = false
weight = 2003
[[resources]]
  src = "26878553260_e9d6667f6f_k.jpg"
  title = "Black and white photos of a wind turbine, viewed from below."
  name = "windturbine"
+++

{{< imgproc name="windturbine"
    command="Resize"
    options="1200x webp"
    imageCaption="Black and white photos of a wind turbine, viewed from below."
    imageURL="https://flickr.com/photos/woolamaloo_gazette/26878553260/in/photolist-SzLEYY-SPvcVt-SPvcnV-SzLEvo-SKTMed-SKTMJ1-r4UbsZ-bUiTU2-GXaR2u-GrRMAA-2kry7bA-5rraNy-daiXty-ro6wgR-r6LzAt-2mC7HMp-bUiV1k-bUiTDX-GrRRmm-bUiTBp-GrYpki-ro6wfP-Hgm4rx-2gzq5ZX-2osWYkX-GXaBs5-bUiU2e-ppVee7-r4UbvV-rodDDB-2mC5tee-2oJBC5d-2oJC6R8-2oJzLm8-2oJwFKB-2mBZ88S-aCVkyT-2mC8SyX-2mC5tGJ-2mC4mP5-2mBZ8YK-2mC4nCz-2mC4nHe-2mBZ8LW-2mC7KFQ-2mC7Jns-2mC8TtY-2mC8Uc6-2mC8U1z-2mC7Ko5"
    imageAttrib="byronv2"
    imageLicense="(CC BY-NC 2.0)"
    licenseURL="https://creativecommons.org/licenses/by-nc/2.0/">}}


## Today's notes {#today-s-notes}

Some reaction to Rachel Reeves' first speech as Chancellor of the Exchequer.

-   [Labour lifts Tories’ ‘absurd’ ban on onshore windfarms](/blog/links/2024/07/08#labour-lifts-tories-absurd-ban-on-onshore-windfarms): In terms of the change we need it's a drop in the warming ocean, but this is good.
-   [Labour’s housing plans will use green belt land twice size of Milton Keynes, expert says](/blog/links/2024/07/08#labour-s-housing-plans-will-use-green-belt-land-twice-size-of-milton-keynes-expert-says): This one's a bit more divisive.
-   [On this day, from the archive](/blog/links/2024/07/08#on-this-day-from-the-archive): What I was listening to in 2005, toxic ships, and Spielberg's film adaption of Minority Report.

<!--more-->

---


### Labour lifts Tories’ ‘absurd’ ban on onshore windfarms {#labour-lifts-tories-absurd-ban-on-onshore-windfarms}

<https://www.theguardian.com/environment/article/2024/jul/08/labour-lifts-ban-onshore-windfarms-planning-policy>

I have been feeling flat about the UK election result, despite the complete trouncing of the Tories.

The result has been described as a 'sandcastle majority' and that about sums it up. It's an impressive majority on the unstable foundations of a low vote share, low turnout and Reform at Labour's heels in many Northern constituences.

This announcement from Rachel Reeves today though is a good one. We can't have infrastructure projects critical in the solutions to the climate crisis blocked by a minority of people worried about the outlook of their homes.

---


### Labour’s housing plans will use green belt land twice size of Milton Keynes, expert says {#labour-s-housing-plans-will-use-green-belt-land-twice-size-of-milton-keynes-expert-says}

<https://www.theguardian.com/society/article/2024/jul/08/labour-housing-plans-green-belt-land-new-towns-david-rudlin>

Some question the need for mass-scale housebuilding when there's already enough housing stock, and the [real cause of the housing crisis](https://www.theguardian.com/lifeandstyle/2024/mar/19/end-of-landlords-surprisingly-simple-solution-to-uk-housing-crisis#comment-166949339) lies in Landlordism.

If new houses are to be build then we need to stop designing car dependency into these new developments. This means family housing in more compact developments that are more community oriented with transport links focused around public transport, walking and cycling.

---


### On this day, from the archive {#on-this-day-from-the-archive}

-   [19 years ago: Dropping the Musical Baton](<https://mattjon.es/blog/2005/07/dropping-the-musical-baton/>): What I was listening to in 2005.
-   [21 years ago: The Ghost Fleet](<https://mattjon.es/blog/2003/07/the-ghost-fleet/>): A note about a Teesside shipyard's controversial contract to decommision some Navy ships from the US.
-   [22 years ago: Post-film](<https://mattjon.es/blog/2002/07/post-film/>): A short review of the Spielberg film Minority Report.


[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"