+++
title = "Notes 10.8.2024"
author = ["Matt Jones"]
date = 2024-08-10
lastmod = 2024-08-11T23:16:02+01:00
tags = ["Sport", "Media", "Politics"]
categories = ["linknotes"]
draft = false
weight = 2001
+++

## Today's notes {#today-s-notes}

-   [History of the 3000m Steeplechase](/blog/links/2024/08/10/#history-of-the-3000m-steeplechase)
-   [Disinformation most active on X, formerly known as Twitter, EU says](/blog/links/2024/08/10/#disinformation-most-active-on-x-formerly-known-as-twitter-eu-says)

<!--more-->

---


### History of the 3000m Steeplechase {#history-of-the-3000m-steeplechase}

<https://en.wikipedia.org/wiki/Steeplechase_(athletics)#History>

There are a number of modern sporting events that have their roots in the idea of a direct race between two points crossing whatever difficult terrain that lies between them.

So a steeplechase was originally a race (on horseback) between the church steeples of neighbouring villages in Ireland.

Cyclocross was originally pretty much the same thing but on a bicycle, with racers throwing their bikes over stone walls and wading through rivers with bike in hand.

All modern steeplechase or cross country events - whether on horse, on foot or on bike - tend to be on artificial circuits now.

The original events perhaps reflect a time when access to the land was easier than it is now.

---


### Disinformation most active on X, formerly known as Twitter, EU says {#disinformation-most-active-on-x-formerly-known-as-twitter-eu-says}

<https://www.bbc.co.uk/news/technology-66926080>

This article is nearly a year old as I'm writing this, but events in the last few weeks have really validated it.

Every right thinking, sensible person just needs to delete their 'X' account and every mass media organisation needs to divest from it. Does, for example, the BBC have a presence on Parler, Truth Social, Gab? No, so why is it so invested in the equally toxic hell-hole 'X'.

---


[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"