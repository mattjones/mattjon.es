+++
title = "Notes 4.8.2023"
author = ["Matt Jones"]
date = 2023-08-04
lastmod = 2024-08-11T23:16:02+01:00
tags = ["Environment", "Politics"]
categories = ["linknotes"]
draft = false
weight = 2004
[[resources]]
  src = "27437841887_68b825ce4b_c.jpg"
  title = "Photo of a derelicy playground"
  name = "playground"
+++

{{< imgproc name="playground"
    command="Resize"
    options="1200x webp"
    imageCaption="Photo of a rusting see-saw in an overgrown playground. Image converted to monochrome from the original."
    imageURL="https://www.flickr.com/photos/141333312@N03/27437841887/"
    imageAttrib="Trevor Marron"
    imageLicense="(CC BY 2.0)"
    licenseURL="https://creativecommons.org/licenses/by/2.0/">}}


## Today's note {#today-s-note}

Thoughts on how we're denying children the natural right to play outside.

<!--more-->

---


### England’s playgrounds crumble as council budgets fall {#england-s-playgrounds-crumble-as-council-budgets-fall}

<https://www.theguardian.com/environment/2023/aug/04/england-playgrounds-crumble-council-budgets-fall>

Play is part of the physical and cognitive development of humans, other mammals and birds.

Juvenile play happens close to parents and in human society close to dwellings in communal spaces. More recently in our history, play happened in the street.

But 'developed' countries spent the latter half of the 20th Century removing these spaces and opportunity for spontanaous play. Instead, spaces for play are demarcated, usually in public parks and away from the streets where cars, both moving and parked, have become dominant.

Now, it seems, even the spaces defined as area for play are no longer maintained because of a lack of money to maintain them. Simply put, this degradation of space is against nature and against the developmental requirements of children. All while the money that should be invested in public space is funnelled into private hands for the benefit of the few.


[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"