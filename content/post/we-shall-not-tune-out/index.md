---
date: 2025-02-26T18:30:00Z
title: We Shall Not Tune Out
---

I'm convinced that without the formation of a progressive alliance to campaign, knock on doors, educate and advocate for a just and democratic society, our descent - spurred ultimately by the climate crisis - to electing an oppressive, selfish and fear-mongering government of autocrats and oligarchs will soon happen in the UK as it has done in the US and in other countries around the world.

When turning to the news and analysis about what is happening in America, I'm seeing a lack of unified, unequivocal condemnation from UK politicians across the political spectrum about what is happening. This seems to be because our politicians want to be seen as aligning with the anti-immigrant, ‘anti-woke’ and otherwise regressive zeitgeist, because that is where the perceived path to continued power lies.  A case in point is Labour's recent adverts designed to mimic Reform in their identity and anti-migrant messaging.

The result then, in my view, is tacit support of the America’s new administration, the [Overton Window](https://en.wikipedia.org/wiki/Overton_window) is lurching to the 'unthinkable' end of the scale, while the world’s most powerful people have successfully convinced everyone that their problems are not because of the transfer of wealth away from them, but because of immigration, international aid and diversity policies. This is when the world’s richest man, in an unelected position of unbridled power, is depriving millions of people around the world of their health and their lives by shutting down USAID in so called efficiency measures.

My response to all this is to tune out, to avoid exposing myself to the dark and endless stream of provocation and wrong-headedness coming from the men tripping on highest level of global power. I ditched my social media accounts a while ago, I now spend more time offline and pursuing my interests - music, photography, cycling - than I have done in the last 25 years, and my mental health is the better for it. 

But now I have a sense that I need to do my bit to help counter the creeping shift towards societal division and hate, to not be a rabbit in the headlights of an oncoming far-right government.  

In both the 2017 and 2019 General Election campaigns, I canvassed as a member of the Labour Party. While both elections were unsuccessful for Labour nationally, the experience taught me that talking to people does work; understanding people’s motivations for thinking the way they do is important. And by talking, it’s possible to inform and reason with people to cut through the flood of mis-information they may be seeing in their daily lives. 

Progressivism brought about the end of slavery, democracy, civil rights, workers rights, universal healthcare and so on. What we are seeing now is an attack on this; the gradual redistribution of power that has been going on for centuries is being reversed under the guise of the ‘war on woke’, and ‘bureaucracy’ - otherwise known as the regulation that tempers the excesses of capitalism - is being metaphorically shredded by a chainsaw. 

So my plan is to talk to people and this will be my line: That decades of progress and relative global peace are being reversed by a handful of billionaire agitators for no other reason than to further enrich themselves and their shareholders. Even worse, this is happening before the backdrop of the climate crisis, which their actions will only catalyse, displacing billions when lands become unliveable, and the feedback loop continues.

I recently visited Jason Kottke’s long running blog [kottke.org](https://kottke.org). Usually an entertaining escape into links and media of interest, Jason has now focused his output into what is going on in Washington, treating it with the level of crisis warranted for such world changing events. He is using his independent platform to spread information, to counter the misinformation rife on the algorithmic social media platforms.  

It is this that has inspired me to take my head out of the sand, write this blog and do what I can to inform and advocate for a better, more just world.

Because, to paraphrase John Stuart Mill: *"Bad men need nothing more to compass their ends, than that good people should look on and do nothing."*

Difficult as it is, we cannot tune out. 
