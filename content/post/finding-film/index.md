---
title: Finding Film
date: 2024-08-24T10:12:05Z
tags:
- Photography
resources:
- name: two
  src: 2.jpg
- name: four
  src: 4.jpg
- name: six
  src: 6.jpg
- name: five
  src: 5.jpg
imageCaption: Photo of a small decrepit farm building. On right hand side, the words 'Lambs have been attacked have been written'
imageAttrib: Matt Jones
imageLicense: CC BY-NC-ND 2.0
licenseURL: https://creativecommons.org/licenses/by-nc-nd/2.0/
---


{{< imgproc name="four" 
 imageCaption="Woods near Hockenhull Platts"
    imageAttrib="Matt Jones"
    imageLicense="(CC BY 2.0)"
    licenseURL="https://creativecommons.org/licenses/by/2.0/"
    >}}

{{< imgproc name="two"
  imageCaption="Footpath, Flag Lane"
    imageAttrib="Matt Jones"
    imageLicense="(CC BY 2.0)"
    licenseURL="https://creativecommons.org/licenses/by/2.0/" >}}

{{< imgproc name="six"  imageCaption="Sea-wall Steps, Leasowe"
    imageAttrib="Matt Jones"
    imageLicense="(CC BY 2.0)"
    licenseURL="https://creativecommons.org/licenses/by/2.0/" >}}


{{< imgproc name="five"  imageCaption="Bridges at Hockenhull Platts"
    imageAttrib="Matt Jones"
    imageLicense="(CC BY 2.0)"
    licenseURL="https://creativecommons.org/licenses/by/2.0/" >}}