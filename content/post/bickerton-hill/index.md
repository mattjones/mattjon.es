---
type: post
comments: true
date: 2025-02-27T19:00:41Z
resources:
- name: bickerton
  src: bickerton.jpg
tags:
- Photography
title: Bickerton Hill
---

{{< imgproc name="bickerton" 
 imageCaption="Bickerton Hill"
    imageAttrib="Matt Jones"
    imageLicense="(CC BY 2.0)"
    licenseURL="https://creativecommons.org/licenses/by/2.0/"
    >}}