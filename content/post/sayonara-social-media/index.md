---
date: 2024-07-07T16:00:00Z
tags:
title: Sayonara Social Media
---

I deleted my Twitter account in November 2022, after Elon Musk hilariously carried a sink into Twitter HQ, having sensibly purchased Twitter for forty quintillion dollars. Let that sink in.

I just couldn't bear being a member of a platform owned by Prime Tech Doofus Musk, despite having lots of Twitter friends, the glue of those virtual friendships being mainly the politics of cycling infrastructure.

Instead I found [Mastodon](https://joinmastodon.org/) offered a more pleasant experience free of algorithms and full of interesting people posting interesting things.

Although I did something that was classic me; instead of finding and joining an existing instance (or to use an analogy, an island) of like-minded folk, i created my own island with just me as its sole inhabitant. A castaway shouting out to sea with the hope that the tech on which island ran would allow me to be discovered by other islands of like-minded folk. 

But that didn't really happen, and as a clone of Twitter, I had the same problem with it; that I found myself posting stuff just to seek that dopamine hit, the attention, the validation. When that didn't come, my feelings were negative and my mental health took a small inverse hit.

[ActivityPub](https://en.wikipedia.org/wiki/ActivityPub) is an important open protocol because it allows social media platforms to be created that are decentralised and not owned by anyone. But I'm not convinced that social media isn't fundamentally - whether it uses an open protocol or not - a corrosive model of online engagement.

So now, I've decided to make these pages, again, my one online home. Just like it was when I started writing this blog 24 years ago. And with that I'm going to work on my relationships 'in real life', and be more reliant on those dopamine hits from offline human interactions.

