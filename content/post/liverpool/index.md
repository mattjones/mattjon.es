---
title: Photographing Liverpool Metropolitan Cathedral  
date: 2024-10-30T07:12:05Z
tags:
- Photography
resources:
- name: one
  src: lmc1.jpg
- name: two
  src: lmc2.jpg
- name: three
  src: lmc3.jpg
- name: five
  src: 5.jpg
---

My workplace is close to Liverpool Metropolitan Cathedral, and being someone who appreciates interesting architecture, I often gravitate towards it during my lunch break, sometimes with camera in my hand. 

Designed by Frederick Gibberd, it was built between 1962 and 1967 on top of what remains of the first attempt to build a catholic cathedral on the site - now known as Luyten's Crypt - itself built on the site of a large Victorian workhouse.

{{< imgproc name="one" 
 imageCaption="Architectural detail of Liverpool Metropolitan Cathedral"
    imageAttrib="Matt Jones"
    imageLicense="(CC BY 2.0)"
    licenseURL="https://creativecommons.org/licenses/by/2.0/"
    >}}

The modern cathedral was built comparatively cheaply, leading to problems with the fabric of the building almost immediately, resulting in Gibberd being sued for £1.3 Million. I was surprised when I learned this. When you're nearby it, it feels like such a solid, confident piece of architecture.

I took pictures close up to it, capturing the lines and the detail of the various chapel exteriors that protrude around circumference of the building.  

{{< imgproc name="two"
  imageCaption="Architectural detail of Liverpool Metropolitan Cathedral"
    imageAttrib="Matt Jones"
    imageLicense="(CC BY 2.0)"
    licenseURL="https://creativecommons.org/licenses/by/2.0/" >}}

It was a sunny day with a cloudless sky, so not ideal for creating negatives with a broad tonal range. The fact that I didn't have my light meter and had to use a phone app for the task didn't help. I subsequently realised it was giving readings leading to underexposure by two stops. I tend to shoot one stop over the film's rated ISO speed anyway, so I just needed to push process the film by one stop to correct things. 

My Leica MDa is a camera originally designed for scientific use while mounted onto microscopes, so it doesn't have a viewfinder, rangefinder (focus), or light meter. As such it's a slightly cheaper way to get into the Leica M system but requires a slower, more methodical process to use it, which I like.   

{{< imgproc name="three"  imageCaption="Architectural detail of Liverpool Metropolitan Cathedral"
    imageAttrib="Matt Jones"
    imageLicense="(CC BY 2.0)"
    licenseURL="https://creativecommons.org/licenses/by/2.0/" >}}

The film is Ilford HP5+ and the lens is a 28mm Voigtlander Ultron. The film was hand processed using Bellini Hydofen developer and scanned using Nikon LS-50 negative scanner, with a small amount of cropping and spot removal in Lightroom.

The images are published under a Creative Commons license. Email me at [mail@mattjon.es](mailto:mail@mattjon.es) if you would like high resolution versions.
