---
title: Four photographs from 2024
date: 2025-02-06T22:00:05Z
tags:
- Photography
resources:
- name: one
  src: 1.jpeg
- name: two
  src: 2.jpeg
- name: three
  src: 3.jpeg
- name: four
  src: 4.jpeg
imageCaption: Photo of a small decrepit farm building. On right hand side, the words 'Lambs have been attacked have been written'
imageAttrib: Matt Jones
imageLicense: CC BY-NC-ND 2.0
licenseURL: https://creativecommons.org/licenses/by-nc-nd/2.0/
---


{{< imgproc name="one" 
 imageCaption="Marty in the woods 1"
    imageAttrib="Matt Jones"
    imageLicense="(CC BY 2.0)"
    licenseURL="https://creativecommons.org/licenses/by/2.0/"
    >}}

{{< imgproc name="two"
  imageCaption="Tree in the mist"
    imageAttrib="Matt Jones"
    imageLicense="(CC BY 2.0)"
    licenseURL="https://creativecommons.org/licenses/by/2.0/" >}}

{{< imgproc name="three"  imageCaption="Marty in the woods 2"
    imageAttrib="Matt Jones"
    imageLicense="(CC BY 2.0)"
    licenseURL="https://creativecommons.org/licenses/by/2.0/" >}}


{{< imgproc name="four"  imageCaption="Marty in the woods 3"
    imageAttrib="Matt Jones"
    imageLicense="(CC BY 2.0)"
    licenseURL="https://creativecommons.org/licenses/by/2.0/" >}}