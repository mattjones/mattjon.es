---
date: 2025-02-06T19:00:00Z
title: Starship Earth
---

Buckminster Fuller wrote a book called '[Operating Manual For Spaceship Earth](https://en.wikipedia.org/wiki/Operating_Manual_for_Spaceship_Earth)' based on the worldview that we are all the crew of Earth and  we need to work together for the good of everyone and the ship itself.

I fear that we are badly off course, the ship is failing, and we desperately need to consult the manual again.